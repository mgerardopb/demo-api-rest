package com.apirest.controller;

import com.apirest.model.Cliente;
import com.apirest.model.Cuenta;
import com.apirest.model.CuentaResumen;
import com.apirest.service.ClienteService;
import com.apirest.service.CuentaService;
import com.apirest.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping
public class ClienteCuentasController {

    @Autowired
    ClienteService clienteService;

    @Autowired
    CuentaService cuentaService;

    @PostMapping(Constants.APIREST_URL_CLIENTES+"/{documento}/cuentas")
    public ResponseEntity agregarCuenta(@PathVariable String documento,
                                        @RequestBody Cuenta cuenta) {
        Cliente cliente = clienteService.obtenerCliente(documento);
        if (cliente == null) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
        Cuenta cuentaAux = cuentaService.obtenerCuenta(cuenta.getNumero());
        if (cuentaAux == null) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.clienteService.agregarCuenta(documento, cuenta.getNumero());
        return new ResponseEntity<>("Cuenta agregada correctamente.", HttpStatus.CREATED);
    }

    @GetMapping(Constants.APIREST_URL_CLIENTES+"/{documento}/cuentas")
    public CollectionModel obtenerCuentas(@PathVariable String documento) {
        Cliente cliente = clienteService.obtenerCliente(documento);
        if (cliente == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente no encontrado.");
        }
        List<CuentaResumen> cuentas = this.clienteService.obtenerCuentas(documento);
        List<EntityModel> cuentasModel = cuentas.stream().map(x -> {
            Link self = linkTo(methodOn(CuentaController.class).obtenerCuenta(x.getNumero())).withSelfRel();
            return EntityModel.of(x, self);
        }).collect(Collectors.toList());
        Link self = linkTo(methodOn(this.getClass()).obtenerCuentas(documento)).withSelfRel();
        return CollectionModel.of(cuentasModel, self);
    }

    @GetMapping(Constants.APIREST_URL_CUENTAS+"/{numero}/cliente")
    public ResponseEntity obtenerCliente(@PathVariable String numero) {
        Cuenta cuenta = cuentaService.obtenerCuenta(numero);
        if (cuenta == null) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
        List<Cliente> clientes = this.clienteService.obtenerClientes();
        return ResponseEntity.ok(this.cuentaService.obtenerCliente(numero, clientes));
    }
}
