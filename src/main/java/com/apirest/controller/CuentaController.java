package com.apirest.controller;

import com.apirest.model.Cuenta;
import com.apirest.service.CuentaService;
import com.apirest.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(Constants.APIREST_URL_CUENTAS)
public class CuentaController {

    @Autowired
    CuentaService cuentaService;

    @GetMapping
    public List<Cuenta> obtenerCuentas() {
        return cuentaService.obtenerCuentas();
    }

    @GetMapping("/{numero}")
    public ResponseEntity obtenerCuenta(@PathVariable String numero) {
        Cuenta cuenta = cuentaService.obtenerCuenta(numero);
        if (cuenta == null) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(cuenta);
    }

    @PostMapping
    public ResponseEntity insertarCuenta(@RequestBody Cuenta cuenta) {
        cuentaService.insertarCuenta(cuenta);
        return new ResponseEntity<>("Cuenta creado correctamente.", HttpStatus.CREATED);
    }

    @PutMapping("/{numero}")
    public ResponseEntity reemplazarCuenta(@PathVariable String numero, @RequestBody Cuenta cuenta) {
        Cuenta cuentaAux = cuentaService.obtenerCuenta(numero);
        if (cuentaAux == null) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
        cuenta.setNumero(numero);
        cuentaService.guardarCuenta(cuenta);
        return new ResponseEntity<>("Cuenta actualizado correctamente.", HttpStatus.OK);
    }

    @PatchMapping("/{numero}")
    public ResponseEntity emparcharCuenta(@PathVariable String numero, @RequestBody Cuenta cuenta) {
        Cuenta cuentaAux = cuentaService.obtenerCuenta(numero);
        if (cuentaAux == null) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
        cuenta.setNumero(numero);
        cuentaService.emparcharCuenta(cuenta);
        return new ResponseEntity<>("Cuenta actualizado correctamente.", HttpStatus.OK);
    }

    @DeleteMapping("/{numero}")
    public ResponseEntity deleteCuenta(@PathVariable String numero) {
        Cuenta cuentaAux = cuentaService.obtenerCuenta(numero);
        if (cuentaAux == null) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
        cuentaService.borrarCuenta(numero);
        return new ResponseEntity<>("Cuenta eliminado correctamente.", HttpStatus.OK);
    }
}
