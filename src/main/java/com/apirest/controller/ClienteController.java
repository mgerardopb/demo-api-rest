package com.apirest.controller;

import com.apirest.model.Cliente;
import com.apirest.service.ClienteService;
import com.apirest.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Constants.APIREST_URL_CLIENTES)
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @GetMapping
    public CollectionModel obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
        List<Cliente> clientes = clienteService.obtenerClientes(pagina - 1, cantidad);
        List<EntityModel> clientesModel = clientes.stream().map(x -> {
            Link self = linkTo(methodOn(this.getClass()).obtenerCliente(x.getDocumento())).withSelfRel();
            Link cuentas = linkTo(methodOn(ClienteCuentasController.class).obtenerCuentas(x.getDocumento())).withRel("cuentas");
            return EntityModel.of(x, self, cuentas);
        }).collect(Collectors.toList());
        Link self = linkTo(methodOn(this.getClass()).obtenerClientes(pagina, cantidad)).withSelfRel();
        return CollectionModel.of(clientesModel, self);
    }

    @GetMapping("/{documento}")
    public EntityModel obtenerCliente(@PathVariable String documento) {
        Cliente cliente = clienteService.obtenerCliente(documento);
        if (cliente == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente no encontrado.");
        }
        Link self = linkTo(methodOn(this.getClass()).obtenerCliente(documento)).withSelfRel();
        Link cuentas = linkTo(methodOn(ClienteCuentasController.class).obtenerCuentas(documento)).withRel("cuentas");
        return EntityModel.of(cliente, self, cuentas);
    }

    @PostMapping
    public ResponseEntity insertarCliente(@RequestBody Cliente cliente) {
        clienteService.insertarCliente(cliente);
        var metodoObtenerCliente = methodOn(ClienteController.class).obtenerCliente(cliente.getDocumento());
        URI location = linkTo(metodoObtenerCliente).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{documento}")
    public ResponseEntity reemplazarCliente(@PathVariable String documento, @RequestBody Cliente cliente) {
        Cliente clienteAux = clienteService.obtenerCliente(documento);
        if (clienteAux == null) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
        cliente.setDocumento(documento);
        clienteService.guardarCliente(cliente);
        return new ResponseEntity<>("Cliente actualizado correctamente.", HttpStatus.OK);
    }

    @PatchMapping("/{documento}")
    public ResponseEntity emparcharCliente(@PathVariable String documento, @RequestBody Cliente cliente) {
        Cliente clienteAux = clienteService.obtenerCliente(documento);
        if (clienteAux == null) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
        cliente.setDocumento(documento);
        clienteService.emparcharCliente(cliente);
        return new ResponseEntity<>("Cliente actualizado correctamente.", HttpStatus.OK);
    }

    @DeleteMapping("/{documento}")
    public ResponseEntity deleteCliente(@PathVariable String documento) {
        Cliente clienteAux = clienteService.obtenerCliente(documento);
        if (clienteAux == null) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
        clienteService.borrarCliente(documento);
        return new ResponseEntity<>("Cliente eliminado correctamente.", HttpStatus.OK);
    }
}
