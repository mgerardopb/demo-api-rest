package com.apirest.service;

import com.apirest.model.Cliente;
import com.apirest.model.CuentaResumen;

import java.util.List;

public interface ClienteService {

    List<Cliente> obtenerClientes();

    List<Cliente> obtenerClientes(int pagina, int cantidad);

    Cliente obtenerCliente(String documento);

    void insertarCliente(Cliente cliente);

    void guardarCliente(Cliente cliente);

    void emparcharCliente(Cliente cliente);

    void borrarCliente(String documento);

    void agregarCuenta(String documento, String numeroCuenta);

    List<CuentaResumen> obtenerCuentas(String documento);
}
