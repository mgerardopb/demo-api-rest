package com.apirest.service;

import com.apirest.model.Cliente;
import com.apirest.model.Cuenta;

import java.util.List;

public interface CuentaService {

    List<Cuenta> obtenerCuentas();

    Cuenta obtenerCuenta(String documento);

    void insertarCuenta(Cuenta Cuenta);

    void guardarCuenta(Cuenta Cuenta);

    void emparcharCuenta(Cuenta Cuenta);

    void borrarCuenta(String documento);

    String obtenerCliente(String numero, List<Cliente> clientes);
}
