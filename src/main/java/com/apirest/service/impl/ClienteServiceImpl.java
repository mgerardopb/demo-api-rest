package com.apirest.service.impl;

import com.apirest.model.Cliente;
import com.apirest.model.CuentaResumen;
import com.apirest.service.ClienteService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ClienteServiceImpl implements ClienteService {

    public final Map<String, Cliente> clientes = new ConcurrentHashMap<>();

    @Override
    public List<Cliente> obtenerClientes() {
        return List.copyOf(this.clientes.values());
    }

    @Override
    public List<Cliente> obtenerClientes(int pagina, int cantidad) {
        List<Cliente> totalClientes = List.copyOf(this.clientes.values());
        int indiceInicial = pagina*cantidad;
        int indiceFinal = indiceInicial+cantidad;
        if (indiceFinal > totalClientes.size()) {
            indiceFinal = totalClientes.size();
        }
        return totalClientes.subList(indiceInicial, indiceFinal);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        return this.clientes.get(documento);
    }

    @Override
    public void insertarCliente(Cliente cliente) {
        this.clientes.put(cliente.getDocumento(), cliente);
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        this.clientes.replace(cliente.getDocumento(), cliente);
    }

    @Override
    public void emparcharCliente(Cliente cliente) {
        Cliente clienteActual = this.clientes.get(cliente.getDocumento());

        if(cliente.getNombre() != null)
            clienteActual.setNombre(cliente.getNombre());

        if(cliente.getEdad() != null)
            clienteActual.setEdad(cliente.getEdad());

        if(cliente.getFechaNacimiento() != null)
            clienteActual.setFechaNacimiento(cliente.getFechaNacimiento());

        if(cliente.getTelefono() != null)
            clienteActual.setTelefono(cliente.getTelefono());

        if(cliente.getCorreo() != null)
            clienteActual.setCorreo(cliente.getCorreo());

        if(cliente.getDireccion() != null)
            clienteActual.setDireccion(cliente.getDireccion());

        this.clientes.replace(cliente.getDocumento(), clienteActual);
    }

    @Override
    public void borrarCliente(String documento) {
        this.clientes.remove(documento);
    }

    @Override
    public void agregarCuenta(String documento, String numero) {
        //final Cliente cliente = this.obtenerCliente(documento);
        CuentaResumen cuenta = new CuentaResumen();
        cuenta.setNumero(numero);
        this.obtenerCliente(documento).getCuentas().add(cuenta);
    }

    @Override
    public List<CuentaResumen> obtenerCuentas(String documento) {
        //final Cliente cliente = this.obtenerCliente(documento);
        return this.obtenerCliente(documento).getCuentas();
    }
}
