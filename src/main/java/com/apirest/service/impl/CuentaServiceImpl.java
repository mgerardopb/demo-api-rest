package com.apirest.service.impl;

import com.apirest.model.Cliente;
import com.apirest.model.Cuenta;
import com.apirest.service.CuentaService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class CuentaServiceImpl implements CuentaService {

    public final Map<String, Cuenta> cuentas = new ConcurrentHashMap<>();

    @Override
    public List<Cuenta> obtenerCuentas() {
        return List.copyOf(cuentas.values());
    }

    @Override
    public Cuenta obtenerCuenta(String documento) {
        return this.cuentas.get(documento);
    }

    @Override
    public void insertarCuenta(Cuenta cuenta) {
        this.cuentas.put(cuenta.getNumero(), cuenta);
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        this.cuentas.replace(cuenta.getNumero(), cuenta);
    }

    @Override
    public void emparcharCuenta(Cuenta cuenta) {
        Cuenta cuentaActual = this.cuentas.get(cuenta.getNumero());

        if(cuenta.getMoneda() != null)
            cuentaActual.setMoneda(cuenta.getMoneda());

        if(cuenta.getSaldo() != null)
            cuentaActual.setSaldo(cuenta.getSaldo());

        if(cuenta.getTipo() != null)
            cuentaActual.setTipo(cuenta.getTipo());

        if(cuenta.getEstado() != null)
            cuentaActual.setEstado(cuenta.getEstado());

        if(cuenta.getOficina() != null)
            cuentaActual.setOficina(cuenta.getOficina());

        this.cuentas.replace(cuenta.getNumero(), cuentaActual);
    }

    @Override
    public void borrarCuenta(String documento) {
        this.cuentas.remove(documento);
    }

    @Override
    public String obtenerCliente(String numero, List<Cliente> clientes) {
        Cliente cliente = clientes.stream().filter(x -> x.getCuentas().contains(numero)).findFirst().orElse(new Cliente());
        return cliente.getDocumento();
    }
}
