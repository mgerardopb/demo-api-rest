package com.apirest.util;

public class Constants {

    public static final String APIREST_URL_BASE = "/api/v1";
    public static final String APIREST_URL_CLIENTES = APIREST_URL_BASE+"/clientes";
    public static final String APIREST_URL_CUENTAS = APIREST_URL_BASE+"/cuentas";
}
